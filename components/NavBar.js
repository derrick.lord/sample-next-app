import React, {Component} from 'react';
import {Link, routes} from '../routes';

import '../css/components/NavBar.css';


class NavBar extends Component{
    constructor(){
        super()
        this.state = {
            dynamicRoutes: [],
        }
    }

    componentWillMount(){
        let updatedRoutes =[];
        routes.forEach((route)=>{
            if(route.pattern.split('/').length <= 2 && route.name !== 'notFound'){
                updatedRoutes.push({
                    name: route.name,
                    path: route.pattern,
                    requireParam: false
                })
            }    
            
            if(route.pattern.split('/').length > 2 && route.name !== 'notFound'){
                const pattern = `/${route.pattern.split('/')[1]}/`;
                updatedRoutes.push({
                    name: route.name,
                    path: pattern,
                    requireParam: true
                })
            }
        });

        this.setState({dynamicRoutes: updatedRoutes});
    }

    generateParam = ()=> Math.floor(Math.random() * 20);

    renderNav = ()=>{
        if(this.state.dynamicRoutes.length > 0){
            return (
                <div>
                    <ul>
                        {this.state.dynamicRoutes.map((route, idx) => {
                            let path = route.path
                            let param = (route.requireParam)?`${this.generateParam()}`:'';
                            let generatedRoute = (path+param);
                            return (
                                <li key={idx}>
                                    <Link to={generatedRoute}>
                                        <a href="/#">{route.name.toUpperCase()}</a>
                                    </Link>
                                </li>
                            )
                        })}
                    </ul>             
                </div>

            )
        }else{
            return <div/>
        }
    }

    render(){
        return(
            <div>
                {this.renderNav()}
            </div>
 
        )
    }
}

export default NavBar;