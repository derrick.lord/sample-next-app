import Head from 'next/head';
import NavBar from '../components/NavBar';

// https://bootswatch.com/4/yeti/bootstrap.min.css

const Layout = ( props ) => {
    return (
        <div>
            <Head>
                <title>Sample Next App</title>
                <link rel="stylesheet" href="https://bootswatch.com/4/yeti/bootstrap.min.css"/>
            </Head>
            <NavBar/>
            <div id="main" className="container">
                {props.children}
            </div>
        </div>
    )
}

export default Layout;
