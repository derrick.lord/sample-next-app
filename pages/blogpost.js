import React, {Component} from 'react';
import { Link } from '../routes';
import Layout from '../components/Layout';
import { withRouter } from 'next/router';

class BlogPost extends Component{
    render(){
        const uid = this.props.router.query.uid;
        return (
            <Layout>
                <h1>Blog page UID: {uid}</h1>
            </Layout>
        )
    }
}

export default withRouter(BlogPost);