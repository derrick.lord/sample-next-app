import React from 'react';
import Layout from '../components/Layout';
import { withRouter } from 'next/router';

const Product = () => (
    <Layout>
       <h1>Product page</h1>
    </Layout>
);

export default withRouter(Product);