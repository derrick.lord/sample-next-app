import React from 'react';
import Layout from '../components/Layout';
import '../css/app.css';

export default () => (
    <Layout>
       <h1 className="red-text">Sample Next App</h1>
    </Layout>
);

