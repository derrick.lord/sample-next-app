const routes = require('next-routes');

module.exports = routes()
    .add('index', '/')
    .add('products', '/products')
    .add('product', '/products/:uid')
    .add('blogHome', '/blog')
    .add('blogpost', '/blog/:uid')
    .add('preview', '/preview')
    .add('notFound', '/*');


